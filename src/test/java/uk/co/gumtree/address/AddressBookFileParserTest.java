package uk.co.gumtree.address;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AddressBookFileParserTest {

	private AddressBookFileParser addressBookFileParser;
	private Contact contact;
	
	@Before
	public void setup() {
		addressBookFileParser = new AddressBookFileParser();
		ReflectionTestUtils.setField(addressBookFileParser, "filepath", "./src/test/resources/AddressBook");
		Date midnight = (new LocalDate().toDateTimeAtStartOfDay( AddressBookService.JODA_BRITISH_TIME_ZONE )).toDate();
		contact = new Contact("Bill McKnigh", midnight, Gender.MALE);
	}
	
	
	@Test
	public void testGetAddressBookFromFile() throws URISyntaxException, IOException {
		Paths.get(ClassLoader.getSystemResource("AddressBook").toURI());
		final Stream<Contact> contactStream = addressBookFileParser.getContactStream();
		assertEquals(5, contactStream.count());
	}
	
	@Test
	public void testLineContactParserWhitespace() {
		assertNull(addressBookFileParser.parse("  "));
	}

	@Test
	public void testLineContactParserBadData() {
		assertNull(addressBookFileParser.parse("Bill Jones, FEMALE, ??/??/??"));
	}

	@Test
	public void testLineContactParserOk() {
		final String lineContact = (new StringBuilder(contact.getName()).
				append(", ").
				append(contact.getGender()).
				append(",").
				append(AddressBookFileParser.DATE_TIME_FORMATTER.format(contact.getDob()))).toString();
		final Contact contact = addressBookFileParser.parse(lineContact);
		//assertEquals(contact, addressBook.findByName(contact.getName()).orElseThrow(() -> new NotFoundException()));
	}

}
