package uk.co.gumtree.address;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class ContactTest {

	private Contact addressBook;
	private String name = "name";
	private Date dob = null;
	private Gender gender = Gender.MALE;
	
	@Before
	public void setup() throws ParseException {
		dob = AddressBookFileParser.DATE_TIME_FORMATTER.parse("26/02/15");
		addressBook = new Contact(name, dob, gender);
	}
	
	@Test
	public void testConstructor() {
		assertEquals(name, addressBook.getName());
		assertEquals(dob, addressBook.getDob());
		assertEquals(gender, addressBook.getGender());
	}
	
	@Test
	public void testHashCodeEquals() {
		
		final Contact ab2 = new Contact(name, dob, gender);
		assertTrue(addressBook.equals(ab2));
		assertTrue(ab2.equals(addressBook));
		assertTrue(ab2.hashCode() == addressBook.hashCode());
		
		final Contact ab3 = new Contact(name+"1", dob, gender);
		assertFalse(addressBook.equals(ab3));
		assertFalse(ab3.hashCode() == addressBook.hashCode());
		
		final Contact ab4 = new Contact(null, dob, gender);
		assertFalse(addressBook.equals(ab4));
		assertFalse(ab4.hashCode() == addressBook.hashCode());
		
		final Contact ab5 = new Contact(name, new Date(), gender);
		assertFalse(addressBook.equals(ab5));
		assertFalse(ab5.hashCode() == addressBook.hashCode());
		
		final Contact ab6 = new Contact(name, null, gender);
		assertFalse(addressBook.equals(ab6));
		assertFalse(ab6.hashCode() == addressBook.hashCode());
		
		final Contact ab7 = new Contact(name, dob, Gender.FEMALE);
		assertFalse(addressBook.equals(ab7));
		assertFalse(ab7.hashCode() == addressBook.hashCode());
		
		final Contact ab8 = new Contact(name, dob, null);
		assertFalse(addressBook.equals(ab8));
		assertFalse(ab8.hashCode() == addressBook.hashCode());
		
		final Contact ab9 = new Contact(name, dob, gender);
		assertTrue(addressBook.equals(ab9));
		assertTrue(ab9.hashCode() == addressBook.hashCode());
		
		
	}
	
	@Test
	public void testToString() {
		assertEquals("AddressBook [name=name, dob=Thu Feb 26 00:00:00 GMT 2015, gender=MALE]", addressBook.toString());
	}
	
}
