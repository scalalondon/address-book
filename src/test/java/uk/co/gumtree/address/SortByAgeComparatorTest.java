package uk.co.gumtree.address;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

public class SortByAgeComparatorTest {

	private Contact oldest;
	private Contact youngest;
	private Contact niddleAged1;
	private Contact niddleAged2;
	
	@Before
	public void setup() {
		oldest = new Contact("Jim Old", (new DateTime(1955,12,11,1,1)).toDate(), Gender.MALE);
		youngest = new Contact("Bill Young", (new DateTime(2000,12,11,1,1)).toDate(), Gender.MALE);
		niddleAged1 = new Contact("Coral Middle", (new DateTime(1980,12,11,1,1)).toDate(), Gender.FEMALE);
		niddleAged2 = new Contact("A Middle", (new DateTime(1983,12,11,1,1)).toDate(), Gender.MALE);
	}
	
	@Test
	public void testComparator() {
		
		final List<Contact> contacts = new ArrayList<Contact>();

		contacts.add(niddleAged1);
		contacts.add(oldest);
		contacts.add(youngest);
		contacts.add(niddleAged2);

		Collections.sort(contacts, new SortByAgeComparator());
		
		assertEquals(oldest, contacts.get(0));
		assertEquals(niddleAged1, contacts.get(1));
		assertEquals(niddleAged2, contacts.get(2));
		assertEquals(youngest, contacts.get(3));
	}

}
