package uk.co.gumtree.address;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;


public class AddressBookApplicationTests {

	@Test
	public void testMain() throws URISyntaxException, IOException {
		URI uri = ClassLoader.getSystemResource("AddressBook").toURI();
		System.out.println(uri.getPath());
		String[] args = new String[]{uri.getPath()};
		AddressBookApplication.main(args);
	}
	
}
