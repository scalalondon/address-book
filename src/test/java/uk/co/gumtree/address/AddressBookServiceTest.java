package uk.co.gumtree.address;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AddressBookServiceTest {

	private AddressBookService addressBookService;

	private List<Contact> contacts;
	private Contact bill;
	private Contact paul;
	private Contact gemma;
	private Contact sarah;
	private Contact wes;
	
	@Before
	public void setup() {
		
		bill = new Contact("Bill McKnight", (new DateTime(1977,3,16,1,1)).toDate(), Gender.MALE);
		paul = new Contact("Paul Robinson", (new DateTime(1985,1,15,1,1)).toDate(), Gender.MALE);
		gemma = new Contact("Gemma Lane", (new DateTime(1991,11,20,1,1)).toDate(), Gender.FEMALE);
		sarah = new Contact("Sarah Stone", (new DateTime(1980, 9,20,1,1)).toDate(), Gender.FEMALE);
		wes = new Contact("Wes Jackson", (new DateTime(1974, 8,14,1,1)).toDate(), Gender.MALE);

		contacts = new ArrayList<Contact>();
		contacts.add(bill);
		contacts.add(paul);
		contacts.add(gemma);
		contacts.add(sarah);
		contacts.add(wes);

		addressBookService = new AddressBookService();
	}
	
	@Test
	public void testCountByGender() {
		assertEquals(3, addressBookService.filterByGender(contacts.stream(), Gender.MALE).size());
	}
	
	@Test
	public void testGetOldestPerson() {
		assertEquals(wes, addressBookService.findOldest(contacts.stream()));
	}
	
	@Test
	public void testFindByName() {
		final Contact contact1 = addressBookService.findByName(contacts.stream(), bill.getName()).orElseThrow(() -> new NotFoundException());
		assertEquals(bill, contact1);
	}

	@Test(expected=NotFoundException.class)
	public void testNotFoundFindByName() {
		addressBookService.findByName(contacts.stream(), "anon").orElseThrow(() -> new NotFoundException());
	}
	
	@Test
	public void testAgeDifferenceBillPaul() {
		final Contact contact1 = addressBookService.findByName(contacts.stream(), bill.getName()).orElseThrow(() -> new NotFoundException());
		final Contact contact2 = addressBookService.findByName(contacts.stream(), paul.getName()).orElseThrow(() -> new NotFoundException());
		assertEquals(2862, addressBookService.findAgeDiffernce(contact1, contact2));

		DateTime dt=new DateTime();
		Contact contactx = new Contact("aName", dt.toDate(), Gender.MALE);
		Contact contacty = new Contact("aName", dt.plusDays(1).toDate(), Gender.MALE);

		assertEquals(1, addressBookService.findAgeDiffernce(contactx, contacty));
		assertEquals(1, addressBookService.findAgeDiffernce(contacty, contactx));

	}
	
}


