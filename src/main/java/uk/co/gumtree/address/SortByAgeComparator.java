package uk.co.gumtree.address;

import java.util.Comparator;


public class SortByAgeComparator implements Comparator<Contact> {

	/**
	 * Date of Birth Comparator
	 */
	@Override
	public int compare(Contact o1, Contact o2) {
		return o1.getDob().compareTo(o2.getDob());
	}

}
