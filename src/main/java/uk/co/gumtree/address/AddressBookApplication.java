package uk.co.gumtree.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@ComponentScan(basePackages = "uk.co.gumtree.address")
@Configuration
public class AddressBookApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(AddressBookApplication.class);
	
    public static void main(String[] args) throws IOException {

		final ConfigurableApplicationContext ctx = SpringApplication.run(AddressBookApplication.class, args);

		final AddressBookFileParser addressBookFileParser = ctx.getBean(AddressBookFileParser.class);
		final AddressBookService addressBookService = ctx.getBean(AddressBookService.class);
		final Stream<Contact> contactStream = addressBookFileParser.getContactStream();

		// convert to a list - can't operate multiple times on stream
		final List<Contact> contacts = contactStream.collect(Collectors.toList());

		LOGGER.info("How many males: {}", addressBookService.filterByGender(contacts.stream(), Gender.MALE).size());
		LOGGER.info("Oldest: {}", addressBookService.findOldest(contacts.stream()).getName());

		final Contact contact1 = addressBookService.findByName(contacts.stream(), "Bill McKnight").orElseThrow(() -> new NotFoundException());
		final Contact contact2 = addressBookService.findByName(contacts.stream(), "Paul Robinson").orElseThrow(() -> new NotFoundException());
		LOGGER.info("Age Difference Between Paul and Bill is {} days", addressBookService.findAgeDiffernce(contact1, contact2));

    }



}
