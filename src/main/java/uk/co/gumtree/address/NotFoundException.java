package uk.co.gumtree.address;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public NotFoundException() {
		super();
	}
	
	/**
	 * 
	 * @param message
	 */
	public NotFoundException(String message) {
		super(message);
	}
	
}
