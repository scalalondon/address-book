package uk.co.gumtree.address;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AddressBookService {

    public static final DateTimeZone JODA_BRITISH_TIME_ZONE = DateTimeZone.forID("Europe/London");
    
	/**
	 *
	 * @param contacts
	 * @param name contact trying to find
	 * @return first matching contact (assume no duplicates)
	 */
	public Optional<Contact> findByName(Stream<Contact> contacts, String name) {
		return contacts.filter(a -> a.getName().toLowerCase().equals(name.toLowerCase())).findFirst();
	}
	
	/**
	 * 
	 * @param gender
	 * @return filtered collection only containing contacts of specified gender
	 */
	public Collection<Contact> filterByGender(Stream<Contact> contacts, Gender gender) {
		return contacts.filter(a -> a.getGender().equals(gender)).collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @return
	 */
	public Contact findOldest(Stream<Contact> contacts) {
		final List<Contact> list = contacts.collect(Collectors.toList());;
		Collections.sort(list, new SortByAgeComparator());
		return list.get(0);
	}
 	
	
	/**
	 *
	 * @param contact1
	 * @param contact2
	 * @return
	 */
	public int findAgeDiffernce(Contact contact1, Contact contact2) {
		final Period period = new Period(new DateTime(contact1.getDob().getTime(), JODA_BRITISH_TIME_ZONE), new DateTime(contact2.getDob().getTime(), JODA_BRITISH_TIME_ZONE), PeriodType.dayTime());
		return Math.abs(period.getDays());
	}

}
