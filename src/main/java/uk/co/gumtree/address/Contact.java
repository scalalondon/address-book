package uk.co.gumtree.address;

import java.io.Serializable;
import java.util.Date;

/**
 * Address Book.
 */
public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Date dob;
	private Gender gender;
	
	/**
	 * 
	 * @param name
	 * @param dob
	 * @param gender
	 */
	public Contact(String name, Date dob, Gender gender) {
		super();
		this.name = name;
		this.dob = dob;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public Date getDob() {
		return dob;
	}

	public Gender getGender() {
		return gender;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Contact)) {
			return false;
		}
		Contact other = (Contact) obj;
		if (dob == null) {
			if (other.dob != null) {
				return false;
			}
		} else if (!dob.equals(other.dob)) {
			return false;
		}
		if (gender != other.gender) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AddressBook [name=" + name + ", dob=" + dob + ", gender="
				+ gender + "]";
	}


}
