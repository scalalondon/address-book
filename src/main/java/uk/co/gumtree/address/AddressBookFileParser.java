package uk.co.gumtree.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Parse a file with following contents...
 * 
 * <pre>
 * Bill McKnight, Male, 16/03/77
Paul Robinson, Male, 15/01/85
Gemma Lane, Female, 20/11/91
Sarah Stone, Female, 20/09/80
Wes Jackson, Male, 14/08/74
</pre>
 */
@Service
public class AddressBookFileParser {

	public static final int EXPECTED_PARTS = 3;
	
	// TODO: how to handle 1900/2000??
	public static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("d/M/yy");

	private static final Logger LOGGER = LoggerFactory.getLogger(AddressBookFileParser.class);

	@Value(value = "${address.book.filepath}")
	private String filepath;


	/**
	 * Given file path, build an AddressBook
	 *
	 * @return stream of lines from the address book
	 * @throws IOException 
	 */
	public Stream<Contact> getContactStream() throws IOException {
		Assert.notNull(filepath);
		final Path path = Paths.get(filepath);
		if(!path.toFile().exists()) {
			throw new IOException("File [" + path + "] doesn't exist, unable to open address book");
		}
		final Stream<String> lines = Files.lines(path, Charset.defaultCharset());
		return lines.map(this::parse).filter(c -> c != null);
	}
	
	/**
	 * Parse line
	 *
	 * @param line
	 * @return parse Contact or null if Parse Exception
	 */
	public Contact parse(String line) {
		try {
			final String[] parts = line.split("\\s*,\\s*");
			final String name = parts[0];
			final Date dob = DATE_TIME_FORMATTER.parse(parts[2]);
			final Gender gender = Gender.valueOf(parts[1].toUpperCase());
			return new Contact(name, dob, gender);
		} catch(Exception e) {
			// continue regardless and log error message
			LOGGER.error("Failed to parse line {}", line, e);
			return null;
		}
	}

}
