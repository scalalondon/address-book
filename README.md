# Gumtree coding challenge

## The application

The application read an AddressBook file and answers the following questions:

1. How many males are in the address book?
2. Who is the oldest person in the address book?
3. How many days older is Bill than Paul?

Notes:

Used SpringBoot to create the uber jar

## To run:

1. mvn package
2. java -jar target/address-book-1.0.jar ./src/test/resources/AddressBook
